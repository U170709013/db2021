#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
import tkinter as tk      #imports the entire Tk package
import mysql.connector as cnt


class Application(tk.Frame):    # App class inherits from Frame class
  def __init__(self, master=None):
    tk.Frame.__init__(self, master)  # Calls constructor for the parent class
    self.pack(fill="both", expand=1) # Make the app appear on the screen    
    self.facility_name = tk.StringVar(self)
    self.createWidgets()
    self.connection = cnt.connect(host='localhost',
                            port='3306',
                            database='project',
                            user='root',
                            password='?')
    self.cursor = self.connection.cursor()
    master.minsize(1278, 720)
    #master.maxsize(2*300, 2*300)  

  
  def createWidgets(self):  # Function responsible for creating all the widgets
    self.list_facility_names = tk.Button(self, bg="yellow") # Creates the button
    self.list_facility_names["text"] = "List facility names" # Set button's parameters
    self.list_facility_names["command"] = self.list_facility
    self.list_facility_names.pack(side="top") # Places the button

    self.hi_there = tk.Button(self,bg="blue") # Creates the button
    self.hi_there["text"] = "List total emissions" # Set button's parameters
    self.hi_there["command"] = self.list_total_emissions
    self.hi_there.pack(side="top") # Places the button

    self.btn_get_location = tk.Button(self, bg="green") # Creates the button
    self.btn_get_location["text"] = "Get location" # Set button's parameters
    self.btn_get_location["command"] = self.get_location_with_facility_name
    self.btn_get_location.pack(side="top") # Places the button

    self.get_facility_name = tk.Entry(self, textvariable=self.facility_name)
    self.get_facility_name.pack(side="right", anchor="ne", padx=30)
    
    self.facility_name_label = tk.Label(self, text="Facility name : ")
    self.facility_name_label.pack(side="right", anchor="ne",padx=10)

    self.location_label = tk.Label(self, text="Latitude : 0\nLongtitude : 0")
    self.location_label.pack(side="right", anchor="ne")
    
    self.list_of_total_emissions = tk.Label(self, text="List of total emissions limited by 20:")
    self.list_of_total_emissions.pack(side="left", anchor="nw", padx=10)

    self.list_of_facility_label = tk.Label(self, text="List of facility limited by 20 :")
    self.list_of_facility_label.pack(side="left", anchor="nw", padx=10)

    self.QUIT = tk.Button(self, text="QUIT", fg="red", command=self.quit)
    self.QUIT.pack(side="bottom")

  def list_total_emissions(self):
    print("run")
    self.cursor.execute("select facilityname, total from facility join" +
                        " total_emissions on (facility.ghgid = total_emissions.facility_ghgid" +
                        " and facility_refyear = total_emissions.facility_refyear) group by facilityname" +
                        " order by total desc limit 20")
    text = ""
    for i in self.cursor:
      print(i)
      print(text)
      text = text + i[0] + " : " + i[1] + "\n"
    self.list_of_total_emissions['text'] = "List of total emissions limited by 20: \n" + text

  def get_location_with_facility_name(self):
    print(self.facility_name.get())
    self.cursor.execute("select laitude, longtitude" +
                        " from geolocation join facility on" +
                        " (geolocation.facility_ghgid = facility.ghgid" +
                        " and geolocation.facility_refyear = facility.refyear)" +
                        " where facility.facilityname = '"+self.facility_name.get()+
                        "' limit 1")
    
    for i in self.cursor:
      self.location_label['text'] = "Laitude : " + str(i[0]) + "\nLongtitude : " + str(i[1])
  
  def list_facility(self):
    print("runned")
    self.cursor.execute("select facilityname from facility group by facilityname limit 20")
    text = ""
    for i in self.cursor:
      print(i)
      print(text)
      text = text + i[0] + "\n"
    self.list_of_facility_label['text'] = "List of facility limited by 20 : \n" + text

  def quit(self):
      self.cursor.close()
      self.connection.close()
      root.destroy()

      
root = tk.Tk()
app = Application(master=root)          # Instantiating the App class
app.master.title("Sample application")  # Sets the title of the window
app.mainloop() # Starts the app's main loop; waiting for mouse and keyboard events







